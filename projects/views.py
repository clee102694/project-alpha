from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from tasks.models import Task
from projects.forms import ProjectForm

# Create your views here.


@login_required(login_url="/accounts/login/")
def list_projects(request):
    project = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": project,
    }
    return render(request, "projects/projects.html", context)


@login_required(login_url="/accounts/login/")
def show_project(request, id):
    project = Task.objects.filter(project_id=id)
    context = {
        "show_project": project,
    }
    return render(request, "projects/details.html", context)


@login_required(login_url="/accounts/login/")
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
